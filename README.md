# Stable Diffusion HDA

This is an experimental digital asset to work with Stable Diffusion and the Hugging Face Diffusers library using Houdini's COP nodes.

The digital asset pretty much follows the example Collab notebook.

[https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/image_2_image_using_diffusers.ipynb](https://colab.research.google.com/github/huggingface/notebooks/blob/main/diffusers/image_2_image_using_diffusers.ipynb)


Setup 
====
- Note this has only been tested on Windows. 
- You'll need to find your installation of Houdini, typically found at `C:\Program Files\Side Effects Software\<houdini version>`
- From there, you'll have to locate the python installation bundled with Houdini that you want to use. The path should now look something like  `C:\Program Files\Side Effects Software\<Houdini version>\<python version>`. The provided python executable is what you will use for the next few steps.
- First install [Pip](https://pip.pypa.io/en/stable/installation/). 
- Then you can install the dependencies mentioned in the notebook.
- You'll also likely have to install PyTorch as well separately in order to take advantage of GPU processing.

UPDATE 7/8/2024
===
While the previous way is still valid, turns out you can alter the system path in script directly. (can't you tell I don't get to work with Python a whole lot haha)

I'm using Anaconda so adjust the path as needed.

```
import sys 
sys.path.append("C:/Users/<username>/anaconda3/envs/<env name>/Lib/site-packages")

# then import whatever 
```

Note here that we're using forward slashes instead of backslashes like Windows paths generally use. 


Notes
===
- I haven't been able to find anything that confirms this but I believe that COPs are run twice, once for the preview and once for the actual render.
- The preview I believe is 128x128 in size.
- This can cause issues becuase one run will error out due to inconsistant number of pixels. 
- This is currently counter-acted by just evaluating things when the resolution is greater than 128x128
- You can also skip the preview by right-clicking on the node and unticking the preview option under flags. 
- Currently only supports public models/checkpoints. 
- Currently unclear if this will work with non NVidia GPUs, a "Device Type" input box is provided though and defaults to the `cuda` option.
- If you have less than 8GB of VRam, make sure to tick the "Toggle Attention Slicing" checkbox, otherwise CUDA will attempt to utilize as much VRam as possible from the very start.




